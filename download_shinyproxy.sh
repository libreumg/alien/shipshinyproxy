#!/bin/bash

function err {
    msg="$1"
    >&2 echo "[ERROR] $(date): $msg"
}

function msg {
    msg="$1"
    >&2 echo "[MESSAGE] $(date): $msg"
}

function wrn {
    msg="$1"
    >&2 echo "[WARNING] $(date): $msg"
}

function get_file_name {
    curl -s https://www.shinyproxy.io/downloads/ | grep -F '<a href="' | grep -F shinyproxy | grep -F .jar | head -n 1 | sed -E 's/^.*(shinyproxy-[0-9][0-9.]*\.jar).*$/\1/'
}

function get_file_url {
    fn="$1"
    if [ -z "$fn" ] ; then
	fn="$(get_file_name)"
    fi
    echo "https://www.shinyproxy.io/downloads/$fn"
}

fn="$(get_file_name)"

msg "Newest version detect: $fn" # TODO: do some sanity checks on th filename, here

url=$(get_file_url "$fn")

msg "Will download $fn from $url"

curl -s "$url" -o "$fn"

md5fn="$fn.md5"

md5url=$(get_file_url "$md5fn")

msg "Will download $md5fn from $md5url"

curl -s "$md5url" -o "$md5fn"

m5exp=$(cat "$md5fn" | awk '{print $1}')

m5got=$(md5sum $fn | awk '{print $1}')

msg "Expected md5: $m5exp, Downloaded file's md5: $m5got"

if [ "$m5exp" != "$m5got" ] ; then
    err "Checksum error, removing $fn ($m5got), expected $m5exp for $url";
    rm "$fn";
    exit 1;
fi

ln -fs $(pwd)"/$fn" "shinyproxy.jar"

exit 0
